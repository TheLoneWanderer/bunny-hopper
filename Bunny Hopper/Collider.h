#pragma once
#include <SFML\Graphics.hpp>



using namespace sf;

class Collider
{
public:
	Collider(Sprite& body);
	~Collider();

	bool checkCollision(Collider& other, float push);
	void move(float dx, float dy);


public:
	Vector2f getPosition();
	Vector2u getHalfSize();




private:


private:
	Sprite &body;
};

