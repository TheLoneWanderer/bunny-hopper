#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>

#include "Environment.h"
#include "Collider.h"

using namespace sf;

class Player :public Drawable, Transformable
{
public:
	Player();
	~Player();

	static int getCollectedCarrotsAmount();
	static void addCarrots(int howManyCarrotsAdd);
	static void setCarrots(int howManyCarrotsSet);

	void loadPlayerTextures();
	void setPlayerSprite();
	void setPlayerTextureRectCords();

	void update();

	Collider getCollider(Sprite &body);
	
public:
	static bool canJump;
	static bool canEat;
	enum playerStatus { WALK, JUMP, IDLE, FALL, EAT } playerState;
	
	Environment envi;
	Texture playerTexture;
	Sprite playerSprite;

	Vector2f velocity;


private:
	virtual void draw(RenderTarget &target, RenderStates states) const;
	
private:
	bool startGravity=false;
	bool facingRight;

	float speed = 8.0f;
	float jumpHeight = 50.0f;
	float gravityForce = 4.0f;
	
	static int collectedCarrots;

	int textureRectCords[20][4];
	int tabIndex = 0;

	



};

