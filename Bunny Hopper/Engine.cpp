#include "Engine.h"

Engine::Engine(RenderWindow &win)
{
	window = &win;
	window->setMouseCursorVisible(false);
	//window->setFramerateLimit(60);
}

Engine::~Engine()
{
	window->setMouseCursorVisible(true);
}


//--------------------------------------------------------------------------------------------------------------
// Silnik ca�ej gry. Obs�uguje w�a�ciwie wszystko.
void Engine::runEngine()
{
	bool keepPlaying = true;

	// �adowanie tekstur, d�wi�k�w, ustawienia muzyki oraz po tym wszystkim w��czenie samej muzyki
	//-------------------------------------------------------------------------------------------------
		// Audio
		//---------------------------------------------------
			audio.loadMusicFiles();
			audio.loadSoundFiles();
			audio.setMusicFiles(true,100);
			audio.setSoundFiles(100);
		//---------------------------------------------------

		// Czcionka
		//---------------------------------------------------
			environment.loadPointsCounterFont();
			environment.setPointsCounterSettings();
		//---------------------------------------------------

		// Grafika, tekstury, sprite'y
		//---------------------------------------------------
			environment.loadEnvironmentTextures();
			environment.setEnvironmentSpriteTextures();
			environment.setEnvironmentSpritePositions();
		//---------------------------------------------------

		// Player
		//---------------------------------------------------
			player.loadPlayerTextures();
			player.setPlayerSprite();
			player.setPlayerTextureRectCords();
		//---------------------------------------------------

	//-------------------------------------------------------------------------------------------------

	// W��czenie muzyki w tle
		audio.playBackgroundMusic1();

	// G��wna p�tla gry
	//---------------------------------------------------------------------------------------------------------

	Clock clock;

	double lag = 0.0;
	double previous = clock.getElapsedTime().asMilliseconds();

	while (keepPlaying)
	{
		double current = clock.getElapsedTime().asMilliseconds();
		double elapsed = current - previous;
		previous = current;

		lag += elapsed;

		Event evnt;

			if (window->pollEvent(evnt))
			{
				if (Keyboard::isKeyPressed(Keyboard::Escape))
				{
					keepPlaying = false;
					audio.stopBackgroundMusic1();
					Player::setCarrots(0);
				}
				
			}
		
		while (lag >= MS_PER_UPDATE)
		{
			updateStuff();

			// Just for testing
			std::cerr << lag << std::endl;
			lag -= MS_PER_UPDATE;
		}

		drawStuff();

	}
	//---------------------------------------------------------------------------------------------------------
}

//--------------------------------------------------------------------------------------------------------------

// Metoda wywo�uj�ca metody dot. poruszania si�, kolizji, animacji itp. (wszystkiego co musi si� aktualizowa�)
void Engine::updateStuff()
{
		player.update();

		// Sprawdzanie kolizji
		//
		for (int i=0; i<10; i++)
		player.getCollider(player.playerSprite).checkCollision(environment.getCollider(environment.platformSprite[i]), 0.0f);

		for (int i=0; i<4; i++)
		player.getCollider(player.playerSprite).checkCollision(environment.getCollider(environment.invisiblePlatformColliderSprite[i]), 0.0f);

		if (player.getCollider(player.playerSprite).checkCollision(environment.getCollider(environment.carrotSprite), 0.0f) && (Player::canEat == true))
		{
			Player::canEat = false;

			Player::addCarrots(1);
			environment.updatePointsCounter();
			player.playerState = Player::EAT;

			audio.playChewingSound();
			environment.setRandomCarrotPos();

			Player::canEat = true;
		}

	environment.updateCarrot();
}
// Metoda czyszcz�ca, rysuj�ca i wy�wietlaj�ca na scenie obiekty
void Engine::drawStuff()
{
		window->clear();

		window->draw(environment);
		window->draw(player);


		window->display();
	
}









