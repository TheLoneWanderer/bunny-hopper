#pragma once
#include <SFML\Graphics.hpp>

using namespace std;
using namespace sf;


class Game
{
public:
	Game();
	~Game();

	static int getWindowWidth();
	static int getWindowHeight();

	void loadGame();
	void runGame();

private:
	Font mainMenuFont;
	Texture mainMenuBackgroundTexture;
	Sprite mainMenuBackgroundSprite;

	enum GameState { MENU, GAME_ON, GAME_OVER, END };
	GameState state;

	static int WINDOW_WIDTH;
	static int WINDOW_HEIGHT;

private:

	void menu();
	void startEngine();


};

