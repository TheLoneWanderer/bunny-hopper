#pragma once
#include <SFML\Graphics.hpp>

#include "Collider.h"


using namespace sf;

class Environment :public Drawable, Transformable
{

public:
	Environment();
	~Environment();

	void loadEnvironmentTextures();
	void setEnvironmentSpriteTextures();
	void setEnvironmentSpritePositions();
	
	void loadPointsCounterFont();
	void setPointsCounterSettings();
	void updatePointsCounter();

	void setRandomCarrotPos();
	void updateCarrot();

public:
	Collider getCollider(Sprite &body);

	Texture carrotTexture;
	Texture backgroundTexture;
	Texture invisiblePlatformColliderTexture;
	Texture treeTexture;
	Texture platformTexture;

	Sprite carrotSprite;
	Sprite backgroundSprite;
	Sprite invisiblePlatformColliderSprite[4];
	Sprite treeSprite[2];
	Sprite platformSprite[10];

private:
	virtual void draw(RenderTarget &target, RenderStates states) const;
	

private:
	Text pointsCounter;
	Font pointsCounterFont;

	int carrotAnimationDirectionChanger;
	int carrotAnimationCounter;
};



	


