#include "Player.h"

#include "Game.h"

#include <Windows.h>
#include <iostream>


Player::Player()
{
}

Player::~Player()
{
}


//Dodatkowe metody
//---------------------------------------------------------
Collider Player::getCollider(Sprite &body)
{
	return Collider(body);
}

int Player::getCollectedCarrotsAmount()
{
	return collectedCarrots;
}

void Player::addCarrots(int howManyCarrotsAdd)
{
	collectedCarrots += howManyCarrotsAdd;
}

void Player::setCarrots(int howManyCarrotsSet)
{
	collectedCarrots = howManyCarrotsSet;
}
//---------------------------------------------------------

// Zmienne statyczne
//---------------------------------------------------------

bool Player::canJump = true;
bool Player::canEat = true;
int Player::collectedCarrots = 0;

//---------------------------------------------------------



// �adowanie tekstury gracza
void Player::loadPlayerTextures()
{
	if (!playerTexture.loadFromFile("Data/Graphics/rabbit/rabbit.png"))
	{
		MessageBox(NULL, "rabbit.png not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}
}

// Ustawienia tekstury gracza
void Player::setPlayerSprite()
{
	playerTexture.setSmooth(true);
	playerSprite.setTexture(playerTexture);
	playerSprite.setPosition(200.00f, 780.0f);
}

// Ustawienie tzw. texture rectangle coordinates (potrzebne do animacji postaci)
void Player::setPlayerTextureRectCords()
{
	// Koordynaty poszczeg�lnych fragment�w sprite sheeta
	// [n][0],[n][1] to wsp�rz�dne x,y (origin)
	// [n][2],[n][3] to szeroko�� i wysoko�� (wektor)
	//--------------------------------------------------------------------

	// Chodzenie
	//--------------------------------------------------------------------
	textureRectCords[0][0] = 324;	textureRectCords[0][1] = 0;
	textureRectCords[0][2] = 92;	textureRectCords[0][3] = 44;

	textureRectCords[1][0] = 417;	textureRectCords[1][1] = 0;
	textureRectCords[1][2] = 92;	textureRectCords[1][3] = 44;

	textureRectCords[2][0] = 0;		textureRectCords[2][1] = 80;
	textureRectCords[2][2] = 92;	textureRectCords[2][3] = 44;
	//--------------------------------------------------------------------

	// Skakanie
	//--------------------------------------------------------------------
	textureRectCords[3][0] = 0;		textureRectCords[3][1] = 0;
	textureRectCords[3][2] = 80;	textureRectCords[3][3] = 79;

	textureRectCords[4][0] = 81;	textureRectCords[4][1] = 0;
	textureRectCords[4][2] = 80;	textureRectCords[4][3] = 79;

	textureRectCords[5][0] = 162;	textureRectCords[5][1] = 0;
	textureRectCords[5][2] = 80;	textureRectCords[5][3] = 79;

	textureRectCords[6][0] = 243;	textureRectCords[6][1] = 0;
	textureRectCords[6][2] = 80;	textureRectCords[6][3] = 79;
	//--------------------------------------------------------------------

	// Stanie w miejscu
	//--------------------------------------------------------------------
	textureRectCords[7][0] = 0;		textureRectCords[7][1] = 125;
	textureRectCords[7][2] = 52;	textureRectCords[7][3] = 64;
	//--------------------------------------------------------------------

	// Spadanie
	//--------------------------------------------------------------------
	textureRectCords[8][0] = 0;		textureRectCords[8][1] = 255;
	textureRectCords[8][2] = 46;	textureRectCords[8][3] = 70;
	//--------------------------------------------------------------------

	// Jedzenie
	//--------------------------------------------------------------------
	textureRectCords[9][0] = 53;	textureRectCords[9][1] = 125;
	textureRectCords[9][2] = 52;	textureRectCords[9][3] = 64;

	textureRectCords[10][0] = 106;	textureRectCords[10][1] = 125;
	textureRectCords[10][2] = 52;	textureRectCords[10][3] = 64;

	textureRectCords[11][0] = 159;	textureRectCords[11][1] = 125;
	textureRectCords[11][2] = 52;	textureRectCords[11][3] = 64;

	textureRectCords[12][0] = 212;	textureRectCords[12][1] = 125;
	textureRectCords[12][2] = 52;	textureRectCords[12][3] = 64;

	textureRectCords[13][0] = 265;	textureRectCords[13][1] = 125;
	textureRectCords[13][2] = 52;	textureRectCords[13][3] = 64;

	textureRectCords[14][0] = 318;	textureRectCords[14][1] = 125;
	textureRectCords[14][2] = 52;	textureRectCords[14][3] = 64;

	textureRectCords[15][0] = 371;	textureRectCords[15][1] = 125;
	textureRectCords[15][2] = 52;	textureRectCords[15][3] = 64;

	textureRectCords[16][0] = 424;	textureRectCords[16][1] = 125;
	textureRectCords[16][2] = 52;	textureRectCords[16][3] = 64;

	textureRectCords[17][0] = 0;	textureRectCords[17][1] = 190;
	textureRectCords[17][2] = 52;	textureRectCords[17][3] = 64;

	textureRectCords[18][0] = 53;	textureRectCords[18][1] = 190;
	textureRectCords[18][2] = 52;	textureRectCords[18][3] = 64;

	textureRectCords[19][0] = 106;	textureRectCords[19][1] = 190;
	textureRectCords[19][2] = 52;	textureRectCords[19][3] = 64;
	//--------------------------------------------------------------------
}

void Player::update()
{
	using std::cout;
	using std::endl;

	cout << playerSprite.getGlobalBounds().width << " <-- X " << playerSprite.getGlobalBounds().height << " <-- Y" << endl;

		if (playerState == WALK)
			playerState = IDLE;

		velocity.x *= 0.65f;
		velocity.y *= 0.90f;

		if ((Keyboard::isKeyPressed(Keyboard::W)) || (Keyboard::isKeyPressed(Keyboard::A)) || (Keyboard::isKeyPressed(Keyboard::D)))
			startGravity = true;

		// Skrypt poruszania si� postaci
		//-------------------------------------------------------------------
			if (Keyboard::isKeyPressed(Keyboard::D))
			{
				facingRight = true;
				playerState = WALK;

				velocity.x += speed;
			}

			if (Keyboard::isKeyPressed(Keyboard::A))
			{
				facingRight = false;
				playerState = WALK;

				velocity.x -= speed;
			}

			if (Keyboard::isKeyPressed(Keyboard::W) && Player::canJump)
			{
				Player::canJump = false;

				velocity.y = -(jumpHeight + gravityForce);
			}
		//-------------------------------------------------------------------

		// Si�a grawitacji
			if (startGravity && velocity.y < 40.0f)
				velocity.y += gravityForce;

			

		playerSprite.move(velocity.x, velocity.y);



	switch (playerState)
	{
		case WALK:
		{
			if (tabIndex > 2)
				tabIndex = 0;
			
				if (facingRight)
					playerSprite.setTextureRect(IntRect(textureRectCords[tabIndex][0], textureRectCords[tabIndex][1], abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));
				else
					playerSprite.setTextureRect(IntRect((textureRectCords[tabIndex][0]+textureRectCords[tabIndex][2]), textureRectCords[tabIndex][1], -abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));


			if (tabIndex >= 2) tabIndex = 0;
			else tabIndex++;
		} break;

		case JUMP:
		{
			if (tabIndex < 3 || tabIndex > 6)
				tabIndex = 3;

			if (facingRight)
				playerSprite.setTextureRect(IntRect(textureRectCords[tabIndex][0], textureRectCords[tabIndex][1], abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));
			else
				playerSprite.setTextureRect(IntRect((textureRectCords[tabIndex][0] + textureRectCords[tabIndex][2]), textureRectCords[tabIndex][1], -abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));

			if (tabIndex >= 6) tabIndex = 3;
			else tabIndex++;
		} break;

		case IDLE:
		{
			tabIndex = 7;

			if (facingRight)
				playerSprite.setTextureRect(IntRect(textureRectCords[tabIndex][0], textureRectCords[tabIndex][1], abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));
			else
				playerSprite.setTextureRect(IntRect((textureRectCords[tabIndex][0] + textureRectCords[tabIndex][2]), textureRectCords[tabIndex][1], -abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));

		} break;

		case FALL:
		{
			tabIndex = 8;

			if (facingRight)
				playerSprite.setTextureRect(IntRect(textureRectCords[tabIndex][0], textureRectCords[tabIndex][1], abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));
			else
				playerSprite.setTextureRect(IntRect((textureRectCords[tabIndex][0] + textureRectCords[tabIndex][2]), textureRectCords[tabIndex][1], -abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));

		} break;

		case EAT:
		{
			if (tabIndex < 9 || tabIndex > 19)
				tabIndex = 9;

			if (facingRight)
				playerSprite.setTextureRect(IntRect(textureRectCords[tabIndex][0], textureRectCords[tabIndex][1], abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));
			else
				playerSprite.setTextureRect(IntRect((textureRectCords[tabIndex][0] + textureRectCords[tabIndex][2]), textureRectCords[tabIndex][1], -abs(textureRectCords[tabIndex][2]), textureRectCords[tabIndex][3]));

			if (tabIndex >= 19) tabIndex = 9;
			else tabIndex++;
		} break;

	}
}


void Player::draw(RenderTarget &target, RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(playerSprite);
}



