#include "Environment.h"

#include "Game.h"
#include "Player.h"

#include <cstdlib>
#include <ctime>
#include <sstream>
#include <Windows.h>


Environment::Environment()
{
	// Przemno�ona przez -1 przydaje si� do zmienienia kierunku animacji g�ra/d� lub lewo/prawo
	carrotAnimationDirectionChanger = 1;

	// Licznik potrzebny do animacji marchewek
	carrotAnimationCounter = 0;
}

Environment::~Environment()
{
}


// Dodatkowe metody
//---------------------------------------------------------
Collider Environment::getCollider(Sprite &body)
{
	return Collider(body);
}

string toString(int liczba)
{
	stringstream ss;
	ss << liczba;

	return ss.str();
}
//---------------------------------------------------------



// �adowanie tekstur t�a oraz jego element�w
void Environment::loadEnvironmentTextures()
{
	// �adowanie t�a menu g��wnego
	if (!(backgroundTexture.loadFromFile("Data/Graphics/background/rescaled-background.png")))
	{
		MessageBox(NULL, "rescaled-background.png not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	// �adowanie tekstury dolnej g��wnej platformy (dodatkowo, potrzebna do kolizji z pod�o�em)
	if (!invisiblePlatformColliderTexture.loadFromFile("Data/Graphics/background/floor-platform.png"))
	{
		MessageBox(NULL, "floor-platform.png not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	// �adowanie tekstury marchewki
	if (!carrotTexture.loadFromFile("Data/Graphics/entity/carrot1.png"))
	{
		MessageBox(NULL, "carrot1.png not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	// �adowanie tekstury drzew
	if (!(treeTexture.loadFromFile("Data/Graphics/background/tree1.png")))
	{
		MessageBox(NULL, "tree1.png not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	// �adowanie tekstury platform
	if (!(platformTexture.loadFromFile("Data/Graphics/background/platform1.png")))
	{
		MessageBox(NULL, "platform1.png not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}
}

// Ustawienia tekstur t�a oraz jego element�w
void Environment::setEnvironmentSpriteTextures()
{
	// Ustawienie "mi�kkich" tekstur
	treeTexture.setSmooth(true);
	carrotTexture.setSmooth(true);
	platformTexture.setSmooth(true);
	backgroundTexture.setSmooth(true);
	invisiblePlatformColliderTexture.setSmooth(true);

	// Ustawienie tekstury t�a
	backgroundSprite.setTexture(backgroundTexture);

	// Ustawienia platformy - pod�ogi
	for (int i = 0; i < 4; i++)
	{
		invisiblePlatformColliderSprite[i].setTexture(invisiblePlatformColliderTexture);
		invisiblePlatformColliderSprite[i].setOrigin(invisiblePlatformColliderSprite[i].getGlobalBounds().width / 2.0f, invisiblePlatformColliderSprite[i].getGlobalBounds().height / 2.0f);
	}
		// Platforma dolna (pod�oga)
		invisiblePlatformColliderSprite[0].setPosition(Game::getWindowWidth() / 2.0f, (Game::getWindowHeight() / 4.0f)*3.0f + 48.0f);

		// Platformy lewa i prawa (1 - lewa; 2 - prawa;)
		invisiblePlatformColliderSprite[1].rotate(90.0f);
		invisiblePlatformColliderSprite[1].setPosition(Game::getWindowWidth() / 16.0f - 120.0f, Game::getWindowHeight() / 2.0f);

		invisiblePlatformColliderSprite[2].rotate(90.0f);
		invisiblePlatformColliderSprite[2].setPosition(Game::getWindowWidth(), Game::getWindowHeight() / 2.0f);

		// Platforma g�rna (sufit)
		invisiblePlatformColliderSprite[3].setPosition(Game::getWindowWidth() / 2.0f, Game::getWindowHeight()/16.0f -160.0f);
	

	// Ustawienie tekstury, przeskalowanie, ustawienie �rodka marchewki
	carrotSprite.setTexture(carrotTexture);
	carrotSprite.setScale(0.32f, 0.32f);
	carrotSprite.setOrigin(0, carrotSprite.getGlobalBounds().height);

	// Ustawienie tekstury, �rodka i pozycji drzew w tle
	for (int i = 0; i < 2; i++)
	{
		treeSprite[i].setTexture(treeTexture);
		treeSprite[i].setOrigin(treeSprite[i].getGlobalBounds().width / 2, treeSprite[i].getGlobalBounds().height);
		treeSprite[i].setPosition((300.0f + i * 1060.0f), 766.0f);
	}

	// Ustawienie tekstury, skali (wymiar�w) oraz �rodka platform
	for (int i = 0; i < 10; i++)
	{
		platformSprite[i].setTexture(platformTexture);
		platformSprite[i].setOrigin(platformSprite[i].getGlobalBounds().width / 2, platformSprite[i].getGlobalBounds().height / 2);
		platformSprite[i].setScale(0.38f, 0.27f);
	}
}


// Pozycje poszczeg�lnych platform (i ewentualne przeskalowania)
void Environment::setEnvironmentSpritePositions()
{
	// Dwie najni�sze (0 - lewa |-| 1 - prawa)
	platformSprite[0].setPosition(Game::getWindowWidth() / 4.0f * 3.0f - 150.0f, Game::getWindowHeight() / 2.0f + 75.0f);
	platformSprite[1].setPosition(Game::getWindowWidth() / 4.0f + 150.0f, Game::getWindowHeight() / 2.0f + 75.0f);

	// �rodkowa dolna (pomi�dzy drzewami, �rodek ekranu)
	platformSprite[2].setPosition(Game::getWindowWidth() / 2.0f, Game::getWindowHeight() / 2.0f - 50.0f);

	// G�ra - lewo od prawego drzewa (|| - *||)
	platformSprite[3].setPosition((Game::getWindowWidth() / 4.0f * 3.0f) - 160.0f, Game::getWindowHeight() / 3.0f);
	// G�ra - prawo od lewego drzewa (||* - ||)
	platformSprite[4].setPosition(Game::getWindowWidth() / 3.0f + 25.0f, Game::getWindowHeight() / 3.0f);

	// Bezpo�rednio nad lewym drzewem
	platformSprite[5].setPosition((Game::getWindowWidth() / 4.0f) - 180.0f, Game::getWindowHeight() / 4.0f);
	// Bezpo�rednio nad prawym drzewem
	platformSprite[6].setPosition((Game::getWindowWidth() / 4.0f * 3.0f) + 160.0f, Game::getWindowHeight() / 4.0f);

	// Na lewo od tytu�u gry w menu g��wnym
	platformSprite[7].setPosition((Game::getWindowWidth() / 2.0f) - 320.0f, Game::getWindowHeight() / 6.0f - 60.0f);
	// Na prawo od tytu�u gry w menu g��wnym
	platformSprite[8].setPosition((Game::getWindowWidth() / 2.0f) + 320.0f, Game::getWindowHeight() / 6.0f - 60.0f);
	// Bezpo�rednio pod tytu�em gry w menu g��wnym
	platformSprite[9].setPosition(Game::getWindowWidth() / 2.0f, Game::getWindowHeight() / 5.0f - 20.0f);

	// Ustawienie marchewki w losowym miejscu startowym
	setRandomCarrotPos();
}

// �adowanie czcionki licznika punkt�w
void Environment::loadPointsCounterFont()
{
	if (!(pointsCounterFont.loadFromFile("Data/Fonts/Orange_Juice_2.0.ttf")))
	{
		MessageBox(NULL, "Orange_Juice_2.0.ttf font not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}
}

void Environment::setPointsCounterSettings()
{
	pointsCounter.setCharacterSize(60);
	pointsCounter.setFont(pointsCounterFont);
	pointsCounter.setString("Carrots: " + toString(Player::getCollectedCarrotsAmount()));
	pointsCounter.setPosition(invisiblePlatformColliderSprite[1].getGlobalBounds().width / 2.0f + 50.0f, invisiblePlatformColliderSprite[1].getGlobalBounds().height / 4.0f);
}

void Environment::updatePointsCounter()
{
	pointsCounter.setString("Carrots: " + toString(Player::getCollectedCarrotsAmount()));
}

// Zainicjowanie RNG oraz wylosowanie "losowej" pozycji marchewki
void Environment::setRandomCarrotPos()
{
	srand(time(0));

	const int carrotSpawnPlaceRandomTabIndex = rand() % 10;
	int carrotSpawnPosDeviation = rand() % 200 - 100;

	carrotSprite.setPosition(platformSprite[carrotSpawnPlaceRandomTabIndex].getPosition().x + carrotSpawnPosDeviation, platformSprite[carrotSpawnPlaceRandomTabIndex].getPosition().y-65);
}

// Metoda aktualizuj�ca stan marchewki, tzn. animacja oraz gdy zostanie zjedzona to przestawiamy j� w inne miejsce (wywo�ujemy ponownie funkcj� setRandomCarrotPos)
void Environment::updateCarrot()
{
	// Animacja "lewitowania" marchewki
	//-----------------------------------------------------------------------------------------------------------
	carrotAnimationCounter++;

	if (carrotAnimationCounter >= 35)
	{
		carrotAnimationDirectionChanger = -carrotAnimationDirectionChanger;
		carrotAnimationCounter = 0;
	}

	carrotSprite.move(0.0f, 0.5f * carrotAnimationDirectionChanger);

	//-----------------------------------------------------------------------------------------------------------

}

// Za�adowanie obiekt�w do bufora, w celu wy�wietlenia metod� display() (clear -> draw -> display)
void Environment::draw(RenderTarget &target, RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(backgroundSprite);
	target.draw(carrotSprite);
	target.draw(pointsCounter);
	

	// W razie potrzeby sprawdzenia/poprawienia kolizji bok�w/sufitu/pod�ogi -> odkomentuj
	/* 
		for (int i = 0; i < 4; i++)
		target.draw(invisiblePlatformColliderSprite[i]);
	*/
	

	for (int i = 0; i < 2; i++)
		target.draw(treeSprite[i]);

	for (int i = 0; i < 10; i++)
		target.draw(platformSprite[i]);

}





