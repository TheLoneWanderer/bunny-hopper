#include "Game.h"

#include "Engine.h"

#include <cstdlib>
#include <ctime>
#include <string>
#include <Windows.h>

RenderWindow window;

// Definicja rozdzielczo�ci ekranu
int Game::WINDOW_WIDTH = 1680;
int Game::WINDOW_HEIGHT = 1050;


// Stworzenie okna, ustawienia rozdzielczo�ci, za�adowanie czcionki z pliku
Game::Game()
{
	// Stworzenie okna gry
		window.create(VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Bunny Hopper", Style::Close);
		state = END;
}

Game::~Game()
{
}


// Dodatkowe metody
//--------------------------------------------------
	int Game::getWindowWidth()
	{
		return WINDOW_WIDTH;
	}

	int Game::getWindowHeight()
	{
		return WINDOW_HEIGHT;
	}
//--------------------------------------------------


// Stworzenie okna, �adowanie czcionki, przej�cie w stan menu
void Game::loadGame()
{
	// �adowanie t�a menu g��wnego
	if (!(mainMenuBackgroundTexture.loadFromFile("Data/Graphics/menuBackground/menuBackground0.png")))
	{
		MessageBox(NULL, "menuBackground0.png texture not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	} 
		mainMenuBackgroundSprite.setTexture(mainMenuBackgroundTexture);

	// �adowanie czcionki menu g��wnego
	if (!(mainMenuFont.loadFromFile("Data/Fonts/Emily_The_Brush.ttf")))
	{
		MessageBox(NULL, "Emily_The_Brush.ttf font not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	state = MENU;
}

// Sterownik ca�ego programu. Decyduje o tym co wy�wietla� (menu g��wne/gr�/zamkn�� program itp.)
//---------------------------------------------------------------------------------------------------------
void Game::runGame()
{
	loadGame();

	while (state != END)
	{

		switch (state)
		{
			case GameState::MENU:
				menu();
				break;

			case GameState::GAME_ON:
				startEngine();
				break;
		}

	}
}

//---------------------------------------------------------------------------------------------------------

// Menu g��wne (�adowanie tekstu, wy�wietlanie itp.)
void Game::menu()
{

	// Zdefiniowanie tytu�u, ustawienie jego stylu, czcionki, centrum i wy�rodkowanie
	//------------------------------------------------------------------------------------------------
		Text title("Bunny Hopper", mainMenuFont, 65);
		title.setStyle(Text::Italic);

		title.setOrigin(title.getGlobalBounds().width / 2,title.getGlobalBounds().height / 2);
		title.setPosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 12);
	//------------------------------------------------------------------------------------------------


	// Ilo�� opcji w menu, tablica napis�w w nim zawartych
	//------------------------------------------------------------------------------------------------
		const int menuOptionsAmount = 2;

		Text menuOption[menuOptionsAmount];
		string str[] = {"Play","Exit"};
	//------------------------------------------------------------------------------------------------


	// Ustawienie kolejno rodzaju i wielko�ci czcionki, tekstu do wy�wietlenia oraz ustawienie ich centrum i wy�rodkowanie
	//--------------------------------------------------------------------------------------------------------------------------------------
		for (int i=0; i < menuOptionsAmount; i++)
		{
			menuOption[i].setFont(mainMenuFont);
			menuOption[i].setCharacterSize(42);

			menuOption[i].setString(str[i]);

			menuOption[i].setOrigin(menuOption[i].getGlobalBounds().width / 2, menuOption[i].getGlobalBounds().height / 2);
			menuOption[i].setPosition(WINDOW_WIDTH / 2, (WINDOW_HEIGHT / 4) + i * 80);
		}
	//--------------------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------------------------------
	// P�tla g��wna menu (wykonuje si� dop�ki jeste�my w stanie MENU)
		while (state == MENU)
		{
			Vector2f mouse(Mouse::getPosition(window));
			Event evnt;

			while (window.pollEvent(evnt))
			{
					if (evnt.type == Event::Closed || evnt.type == Event::KeyPressed && evnt.key.code == Keyboard::Escape)
						state = END;

					else if (menuOption[0].getGlobalBounds().contains(mouse) && evnt.type == Event::MouseButtonReleased && evnt.key.code == Mouse::Left)
						state = GAME_ON;

					else if (menuOption[1].getGlobalBounds().contains(mouse) && evnt.type == Event::MouseButtonReleased && evnt.key.code == Mouse::Left)
						state = END;
			}

		// Pod�wietlanie opcji w menu po najechaniu na nie kursorem
		//--------------------------------------------------------------------------------
				Color greenColor(0, 153, 0, 255);

				for (int i = 0; i < menuOptionsAmount; i++)
					if (menuOption[i].getGlobalBounds().contains(mouse))
						menuOption[i].setFillColor(Color::Red);

					else menuOption[i].setFillColor(greenColor);
		//--------------------------------------------------------------------------------


		// Wy�wietlanie t�a, tytu�u i opcji w menu
		//--------------------------------------------------------------------------------
				
				window.clear();
				window.draw(mainMenuBackgroundSprite);
				window.draw(title);

			for (int i = 0; i < menuOptionsAmount; i++)
				window.draw(menuOption[i]);
			
				window.display();
		//--------------------------------------------------------------------------------
		
		}
}

// W��czenie w�a�ciwej gry (odpalenie silnika kt�ry obs�uguje scen�, fizyk�, d�wi�k itp.)
void Game::startEngine()
{
	Engine engine(window);
	engine.runEngine();

	state = MENU;
}
