#pragma once
#include <SFML\Graphics.hpp>
#include "Audio.h"
#include "Player.h"
#include "Environment.h"


using namespace sf;

class Engine
{
public:
	Engine(RenderWindow &win);
	~Engine();

	void runEngine();

public:


private:

	void updateStuff();
	void drawStuff();

private:
	float deltaTime;
	const double MS_PER_UPDATE = 1000/60;

	RenderWindow *window;
	Audio audio;
	Environment environment;
	Player player;




};

