#pragma once
#include <SFML\Audio.hpp>

using namespace sf;

class Audio
{
public:
	Audio();
	~Audio();

	void loadMusicFiles();
	void loadSoundFiles();

	void setMusicFiles(bool setLoopBool, float volume);
	void setSoundFiles(float volume);

	void playBackgroundMusic1();
	void stopBackgroundMusic1();

	void playChewingSound();

private:
	Music backgroundMusic1;

	SoundBuffer chewingSoundBuffer[4];
	Sound chewingSound[4];

	


};

