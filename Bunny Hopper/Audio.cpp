#include "Audio.h"

#include <ctime>
#include <cstdlib>
#include <Windows.h>


Audio::Audio()
{
}

Audio::~Audio()
{
}

void Audio::loadMusicFiles()
{

	if (!backgroundMusic1.openFromFile("Data/Music/backgroundMusic1.wav"))
	{
		MessageBox(NULL, "backgroundMusic1.wav not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

}

void Audio::loadSoundFiles()
{

	if (!chewingSoundBuffer[0].loadFromFile("Data/Sounds/carrotChewingSound0.wav"))
	{
		MessageBox(NULL, "carrotChewingSound0.wav not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	if (!chewingSoundBuffer[1].loadFromFile("Data/Sounds/carrotChewingSound1.wav"))
	{
		MessageBox(NULL, "carrotChewingSound1.wav not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	if (!chewingSoundBuffer[2].loadFromFile("Data/Sounds/carrotChewingSound2.wav"))
	{
		MessageBox(NULL, "carrotChewingSound2.wav not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

	if (!chewingSoundBuffer[3].loadFromFile("Data/Sounds/carrotChewingSound3.wav"))
	{
		MessageBox(NULL, "carrotChewingSound3.wav not found. Have you messed with the files? :)", ERROR, NULL);
		return;
	}

}

void Audio::setMusicFiles(bool setLoopBool, float volume)
{
	backgroundMusic1.setLoop(setLoopBool);
	backgroundMusic1.setVolume(volume);
}

void Audio::setSoundFiles(float volume)
{
	for (int i = 0; i < 4; i++)
	{
		chewingSound[i].setBuffer(chewingSoundBuffer[i]);
		chewingSound[i].setVolume(volume);
	}
}


void Audio::playBackgroundMusic1()
{
	backgroundMusic1.play();
}
void Audio::stopBackgroundMusic1()
{
	backgroundMusic1.stop();
}

void Audio::playChewingSound()
{
	srand(time(0));
	int playSoundNr = rand() % 4;

	chewingSound[playSoundNr].play();
}

