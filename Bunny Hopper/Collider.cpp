#include "Collider.h"

#include "Player.h"


Collider::Collider(Sprite& body) :
	body (body)
{
}


Collider::~Collider()
{
}

// Dodatkowe metody
//--------------------------------------------------------------------------------------------------------
	void Collider::move(float dx, float dy)
	{
		body.move(dx, dy);
	}

	Vector2f Collider::getPosition()
	{
		return Vector2f(body.getPosition().x, body.getPosition().y);
	}

	Vector2u Collider::getHalfSize()
	{
		return Vector2u (body.getGlobalBounds().width/2.0f , body.getGlobalBounds().height/2.0f);
	}
//--------------------------------------------------------------------------------------------------------

// Sprawdzenie kolizji i zareagowanie na ni� (zwraca true gdy zachodzi kolizja lub false gdy nie zachodzi)
//-----------------------------------------------------------------------------------------------------------
bool Collider::checkCollision(Collider& other, float push)
{
	Vector2f otherPosition = other.getPosition();
	Vector2u otherHalfSize = other.getHalfSize();
	Vector2f thisPosition = getPosition();
	Vector2u thisHalfSize = getHalfSize();

	float deltaX = otherPosition.x - thisPosition.x;
	float deltaY = otherPosition.y - thisPosition.y;
	float intersectX = abs(deltaX) - (otherHalfSize.x + thisHalfSize.x);
	float intersectY = abs(deltaY) - (otherHalfSize.y + thisHalfSize.y);

	if ((intersectX < 0.0f) && (intersectY < 0.0f))
	{
		push = std::min(std::max(push, 0.0f), 1.0f);

		if (intersectX > intersectY)
		{
			if (deltaX > 0.0f)
			{
				// Collision right
				move(intersectX * (1.0f - push), 0.0f);
				other.move(-intersectX * push, 0.0f);
			}

			else
			{
				// Collision left
				move(-intersectX * (1.0f - push), 0.0f);
				other.move(intersectX * push, 0.0f);
			}
		}
		else
		{
			if (deltaY > 0.0f)
			{
				// Collision bottom
				Player::canJump = true;
				move(0.0f, intersectY * (1.0f - push));
				other.move(0.0f, -intersectY * push);
			}

			else
			{
				// Collision top
				move(0.0f, -intersectY * (1.0f - push));
				other.move(0.0f, -intersectY * push);
			}
		}

		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------------------------------------


























